import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton lMilanoSandwichButton;
    private JButton milanoSandwichAButton;
    private JButton milanoSandwichBButton;
    private JButton hotDogButton;
    private JButton lettuceHotDogButton;
    private JButton icedCoffeeButton;
    private JButton iecdCaffeLatteButton;
    private JButton icedTeaButton;
    private JButton tunaSandButton;
    private JButton blendCoffeeButton;
    private JButton caffeLatteButton;
    private JButton teaButton;
    private JLabel hotDrinkLabel;
    private JLabel icedDrinkLabel;
    private JLabel hotDogsHotSandwichLabel;
    private JLabel milanoSandwichesLabel;
    private JButton checkOutButton;
    private JButton callingStaffButton;
    private JTextPane orderDisplay;
    private JLabel accoutLabel;
    private JLabel orderListLabel;
    private  int total;

    int orderFood(String food, int price, int totalPrice){
        String currentText = orderDisplay.getText();
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?\nIt is "+price+" yen.",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,
                    "Order for "+food+" received.");
            if(totalPrice==0){
                orderDisplay.setText(food+"\n"+"￥"+price+"\n");
            }
            else{
                currentText += food+"\n"+"￥"+price+"\n";
                orderDisplay.setText(currentText);
            }
            totalPrice += price;
            accoutLabel.setText("Total: ￥"+totalPrice);
        }
        return totalPrice;
    }

    int orderDrink(String drink, int price, int totalPrice){
        String currentText = orderDisplay.getText();
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+drink+"?\nIt is "+price+" yen.",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            confirmation = JOptionPane.showConfirmDialog(null,
                    "Size can be changed to larger size for an additional 50 yen.\nwould you like to change?",
                    "Size Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if(confirmation==0){
                JOptionPane.showMessageDialog(null,
                        "Order for "+drink+"(Larger size) received.");
                price += 50;
            }
            else{
                JOptionPane.showMessageDialog(null,
                        "Order for "+drink+" received.");
            }

            if(totalPrice==0){
                orderDisplay.setText(drink+"\n"+"￥"+price+"\n");
            }
            else{
                currentText += drink+"\n"+"￥"+price+"\n";
                orderDisplay.setText(currentText);
            }
            totalPrice += price;
            accoutLabel.setText("Total: ￥"+totalPrice);
        }
        return totalPrice;
    }

    void callStaff(){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to call staff?",
                "Calling Staff Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,
                    "Staff will be there to help you.\nPlease wait a second.");
        }
    }

    int checkOut(int totalPrice){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,
                    "Thank you.\nThe total price is ￥"+totalPrice+".");
            totalPrice = 0;
            accoutLabel.setText("Total: ￥"+totalPrice);
            orderDisplay.setText("No ordered.");
        }
        return totalPrice;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodOrderingMachine() {
        total = 0;
        lMilanoSandwichButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\LMS.jpg")));
        lMilanoSandwichButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderFood("Limited edition Milano sandwich", 590, total);
            }
        });
        milanoSandwichAButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\MSA.jpg")));
        milanoSandwichAButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderFood("Milano sandwich A", 490, total);
            }
        });
        milanoSandwichBButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\MSB.jpg")));
        milanoSandwichBButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderFood("Milano sandwich B", 570, total);
            }
        });
        hotDogButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\HD.jpg")));
        hotDogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderFood("Hot dog", 280, total);
            }
        });
        lettuceHotDogButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\LHD.jpg")));
        lettuceHotDogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderFood("Lettuce hot dog", 320, total);
            }
        });
        tunaSandButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\tuna.jpg")));
        tunaSandButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderFood("Tuna with cheddar cheese", 420, total);
            }
        });
        icedCoffeeButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\I_coffee.jpg")));
        icedCoffeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderDrink("Iced coffee", 300, total);
            }
        });
        iecdCaffeLatteButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\I_latte.jpg")));
        iecdCaffeLatteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderDrink("Iced caffe latte", 390, total);
            }
        });
        icedTeaButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\I_Tea.jpg")));
        icedTeaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderDrink("Iced tea", 310, total);
            }
        });
        blendCoffeeButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\B_coffee.jpg")));
        blendCoffeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderDrink("Blend coffee", 305, total);
            }
        });
        caffeLatteButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\latte.jpg")));
        caffeLatteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderDrink("Caffe latte", 395, total);
            }
        });
        teaButton.setIcon(new ImageIcon(this.getClass().getResource(".\\icons\\Tea.jpg")));
        teaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = orderDrink("Tea", 260, total);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total = checkOut(total);
            }
        });
        callingStaffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                callStaff();
            }
        });
    }
}